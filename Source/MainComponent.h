/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                               MidiInputCallback,
                                Slider::Listener,
                                Button::Listener
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    
    void handleIncomingMidiMessage(MidiInput*, const MidiMessage&) override;
    
    void sliderValueChanged (Slider* slider) override;
    
    void buttonClicked (Button* button) override;

private:
    AudioDeviceManager audioDeviceManager;
    Label midiLabel;
    Slider ChannelSlider;
    Slider Number;
    Slider Velocity;
    MidiMessage midiMsg;
    TextButton play;
    

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
